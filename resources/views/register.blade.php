<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Sign Up Form</h1>
    <form action="welcome" method="post">
        @csrf
        <label for="firstname"> First name : </label> <br><br>
        <input type="text" name="firstname" id=""> <br><br>
        <label for="lastname">Last name : </label> <br><br>
        <input type="text" name="lastname" id=""> <br> <br>
        <label for="">Gender :</label> <br> <br>
        <input type="radio" name="jk" id="male"> 
        <label for="male">Male</label> 
        <input type="radio" name="jk" id="female"> 
        <label for="female">Female</label> <br> <br>
        <label for=""> Nationality : </label> <br> <br>
        <select name="nationality" id="">
            <option value="indo">Indonesian</option>
            <option value="malay">Malaysia</option>
            <option value="australia">Australia</option>
            <option value="singapura">Singapura</option>
        </select> <br> <br>
        <label for="">Language Spoken : </label> <br> <br>
        <input type="checkbox"> <label for="">Bahasa Indonesia</label> <br>
        <input type="checkbox"> <label for="">English</label><br>
        <input type="checkbox"> <label for="">Other</label><br> <br>

        <label for="bio">Bio : </label> <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <button style="cursor: pointer;">Sign Up</button>
        

    </form>
</body>
</html>